//
//  tryingToBeFancyVersion.h
//  karnaugh-map-test
//
//  Created by Douglas Barr on 06/12/2021.
//

#pragma once

// This version was only _really_ included as you will see lots of people who write about
// functional programming talking about using lambdas. While I do use lambdas fairly often
// when I'm writing C++ code, I'm not sure I would use them like this.

bool isLeapYearWithLambdas (const int year)
{
    auto divisibleByFour = [](int year) { return year % 4 == 0; };
    auto divisibleByOneHundred  = [](int year) { return year % 100 == 0; };
    auto divisibleByFourHundred = [](int year) { return year % 400 == 0; };

    auto firstLeapYearCheck = [&divisibleByFour, &divisibleByOneHundred](int year)
    {
        return divisibleByFour(year) && !divisibleByOneHundred(year);
    };
    
    auto secondLeapYearCheck = [&divisibleByFour, &divisibleByFourHundred](int year)
    {
        return divisibleByFour(year) && divisibleByFourHundred(year);
    };
    
    return firstLeapYearCheck(year) || secondLeapYearCheck(year);
}

