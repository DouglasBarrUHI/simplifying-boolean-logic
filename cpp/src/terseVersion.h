//
//  terseVersion.h
//  karnaugh-map-test
//
//  Created by Douglas Barr on 06/12/2021.
//

#pragma once

// This version was done in a way that you will often see online.
// The biggest issue about this that we discussed is how it makes it effectively
// impossible to step through using a debugger, and how much of an issue that
// could be.

bool isLeapYearReallyTerse (int year)
{
    return ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)));
}
