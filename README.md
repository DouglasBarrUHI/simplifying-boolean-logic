# README #

## Context

This project was an example for a discussion of simplifying boolean logic. It came from a discussion of a solution to this challenge https://www.hackerrank.com/challenges/write-a-function/problem that had a lot of nested if statements, and how to simplify them down. 

I made a point in the [discussion](https://uhi.webex.com/uhi/ldr.php?RCID=898c0c0564c2001ab31e8ee5a63bd520) that I would make sure I included a comment block like this at the start of the function:

```
/**A function to work out if a year is a leap year.
*
*   The fuction works out if a year is a leap year using the following rules:
*   In the Gregorian calendar, three conditions are used to identify leap years:
*       - The year can be evenly divided by 4, is a leap year, unless:
*           - The year can be evenly divided by 100, it is NOT a leap year, unless:
*               - The year is also evenly divisible by 400. Then it is a leap year.
*
*   The Karnaugh Map for working out the conditions can be found at:
*       https://myfancycompany.atlassian.net/wiki/spaces/myfancyproduct/pages/1234/leap+year+calculations
*/
```
I've pulled it out into the readme just now for clarity, so that it isn't copied accross all of the different versions of the function, and so that I can include some comments above each version to introduce them and why they are there. 

## Build instructions

At the moment there is a couple of Python versions, but most of the examples are all in C++. I originally wrote them in Xcode as I was at my mac working when the conversation started. I'll add in some cross-platform builds when I have time, but would also welcome some pull requests if anyone wants to have a go at a CMake script or something? 