//
//  tryingToBeFancyVersion.h
//  karnaugh-map-test
//
//  Created by Douglas Barr on 06/12/2021.
//

#pragma once

// I have included this version just for transparency. This is the first
// 'neat' code that I sat down and wrote when I refactored the original version that
// had lots of nested if statements. It's not necessarily the final version that
// I would commit to a project I was working on, but thought it might help to show
// that I didn't jump immediately for any specific approach (functional programming
// or whatever).

bool isLeapYearNeat (const int year)
{
    const bool canBeDividedByFour = (year % 4 == 0);
    const bool canBeDividedByOneHundred = (year % 100 == 0);
    const bool canBeDividedByFourHundred = (year % 400 == 0);

    if (canBeDividedByFour && (! canBeDividedByOneHundred))
    {
        return true;
    }
    else if (canBeDividedByFour && canBeDividedByFourHundred)
    {
        return true;
    }
    else
        return false;
}
