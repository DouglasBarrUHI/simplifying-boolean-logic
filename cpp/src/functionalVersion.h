//
//  functionalVersion.h
//  karnaugh-map-test
//
//  Created by Douglas Barr on 06/12/2021.
//

#pragma once

// This version follows quite a strict 'functional programming' approach.
// We discussed the pros and cons in terms of readability, testability,
// reusability of code, and how easy it would be to debug.

const inline bool isDivisibleByFour(const int number)
{
    return number % 4 == 0;
};

const inline bool isDivisibleByOneHundred(const int number)
{
    return number % 100 == 0;
};

const inline bool isDivisibleByFourHundred(const int number)
{
    return number % 400 == 0;
};

const inline bool isDivisibleByFourButNotOneHundred(const int number)
{
    return (isDivisibleByFour(number) && !isDivisibleByOneHundred(number));
}

const inline bool isDivisibleByFourAndFourHundred(const int number)
{
    return isDivisibleByFour(number) && isDivisibleByFourHundred(number);
}

const bool isLeapYearFunctions(const int year)
{
    return isDivisibleByFourAndFourHundred(year) || isDivisibleByFourAndFourHundred(year);
}
