
def is_leap(year):

    canBeDividedByFour = (year % 4 == 0)
    canBeDividedByOneHundred = (year % 100 == 0)
    canBeDividedByFourHundred = (year % 400 == 0)

    firstLeapYearCheck = (canBeDividedByFour and (not canBeDividedByOneHundred))
    secondLeapYearCheck = (canBeDividedByFour and canBeDividedByFourHundred)
    
    isLeapYear = firstLeapYearCheck or secondLeapYearCheck

    return isLeapYear

year = int(1992)

if (is_leap(year)):
    print ("{} is a leap year".format(year))
else:
    print ("{} is not a leap year".format(year))