//
//  messyVersion.h
//  karnaugh-map-test
//
//  Created by Douglas Barr on 06/12/2021.
//

#pragma once

// This is a version I wrote where I deliberately put in a bunch of nested if statements.
// We discussed:
//  -   what this would demonstrate about your approach to solving a problem like
//      this if you were ever given an interview question like this.
//  -   that this type of code has a high 'cyclomatic complexity' (a good measure of how bug prone it will be)

bool isLeapYearMessy (int year)
{
    if (year % 4 == 0)
    {
        if (year % 100 == 0)
        {
            if (year % 400 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return true;
    }
    else
    {
        return false;
    }
}
