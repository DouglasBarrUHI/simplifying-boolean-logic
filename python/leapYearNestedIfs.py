def is_leap(year):
    leap = False
    if (year % 4 == 0):
        leap = True
        if (year % 100 == 0):
            leap = False
                if (year % 400 == 0):
                    leap = True
    else:
        leap = False
    return leap

year = int(1992)

if (is_leap(year)):
    print ("{} is a leap year".format(year))
else:
    print ("{} is not a leap year".format(year))